# Tabela jedynek {#1}

|Pierwsza_jedynka | Druga_jedynka | Trzecia jedynka |
| :---: | :---: | :---: |
|1	|1	|1	|

[1](#1)


# Plany na piątek {#2} [^1]
- []Wstać
- []Dojechać
- []Przeżyć
- []Wrócić
- []Spać

[2](#2)


# Python {#3}

```py
print("Rdzenie logiczne != Rdzenie fizyczne")
```

[3](#3)


# Git tasks {#4} [^2]

While True:

1. git clone :neutral_face:
2. nano main.py :smile:
3. git commit :relaxed:
4. git error :angry:
5. git rm -r -f repo :dizzy_face:


[4](#4)


Przypisy:
[^1]:Tak wygląda mój Piątek
[^2]:A tak moje zadania z gita

